
/*1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
    - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
    (target.health - this.attack)

2.) If health is below 5, invoke faint function*/


function Pokemon(name, lvl, hp, atk){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = atk;

	this.tackle = function(target){
		console.log(this.name + ' '+ 'tackled ' + target.name)
	};

	this.hpRemaining = function(target){
		this.health = this.health - target.attack;
		console.log(this.name + 's health is now reduced to ' + this.health )
	};

	this.faint = function(){

		if (this.health < 5){console.log(this.name + ' ' + 'fainted');
		}
	

	}
}

	


let meowth = new Pokemon('Meowth', 5, 34, 10);
let chikorita = new Pokemon('Chikorita', 10, 40, 15);

chikorita.tackle(meowth);
meowth.hpRemaining(chikorita);

meowth.tackle(chikorita);
chikorita.hpRemaining(meowth);

chikorita.tackle(meowth);
meowth.hpRemaining(chikorita);


meowth.faint()
chikorita.faint();

